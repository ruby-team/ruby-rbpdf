require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb']
end

task :default => :test_fonts

Rake::TestTask.new(:test_fonts) do |t|
  t.libs = ['rbpdf-font/test']
  t.test_files = FileList['rbpdf-font/test/*_test.rb']
  t.verbose = true
  t.options = '-v'
end
